#include <algorithm>
#include <iostream>
#include <string>
#include <boost/compute.hpp>

namespace compute = boost::compute;


struct DeviceInfo
{
    std::string name;
    std::string profile;
    std::string version;
    std::string driver_version;
    unsigned long global_memory;
    unsigned long local_memory;
    unsigned int clock_frequency;
    unsigned int compute_units;

    explicit DeviceInfo(const compute::device &dev) {
        name = dev.name();
        profile = dev.profile();
        version = dev.version();
        driver_version = dev.driver_version();
        global_memory = dev.global_memory_size();
        local_memory = dev.local_memory_size();
        clock_frequency = dev.clock_frequency();
        compute_units = dev.compute_units();
    }

    inline void print_info() {
        std::cout << "Device name: " << name << "\n"
                  << "Profile: " << profile << "\n"
                  << "Version: " << version << "\n"
                  << "Driver version: " << driver_version << "\n"
                  << "Global memory: " << global_memory << "\n"
                  << "Local memory: " << local_memory << "\n"
                  << "Clock frequency: " << clock_frequency << "\n"
                  << "Compute units: " << compute_units << "\n";
    }
};


int main()
{
    compute::device dev = compute::system::default_device();
    DeviceInfo info(dev);
    info.print_info();

    return 0;
}
