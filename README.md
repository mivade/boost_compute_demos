# Boost.Compute demos

## Prerequisites

### Debian and derivatives

Tested with Debian stretch.

Install the following packages:

* libboost-dev
* ocl-icd-dev (OpenCL development package)
* ocl-icd-libopencl (OpenCL runtime files)

Depending on your hardware, you will need to install specific drivers. If you
have an integrated Intel GPU, you can use this:

* beignet-opencl-icd

## Building

```
mkdir build
cd build
cmake ..
cmake --build .
```

## Included demos

* `info` - prints device info and exits
