#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <random>
#include <string>
#include <vector>

#include <boost/compute/algorithm/transform.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/functional/math.hpp>

namespace compute = boost::compute;

using DartVector = std::vector<std::vector<double>>;

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<double> dist(0, 1);


void print_usage()
{
    std::cout << "Usage: mcpi <cpu|boost> [n_darts]" << std::endl;
}


auto throw_darts(size_t n) -> DartVector
{
    DartVector darts(n);
    std::generate(darts.begin(), darts.end(), []() {
        std::vector<double> dart(2);
        dart[0] = dist(gen);
        dart[1] = dist(gen);
        return dart;
    });
    return darts;
}


double calculate_pi_cpu(DartVector darts)
{
    int count = 0;

    for (const auto &dart: darts) {
        const auto x = dart[0];
        const auto y = dart[1];
        if (std::sqrt(x*x + y*y) < 1) {
            ++count;
        }
    }

    return static_cast<double>(4 * count) / darts.size();
}


double calculate_pi_boost_compute(DartVector darts)
{
    auto dev = compute::system::default_device();
    compute::context context(dev);
    compute::command_queue queue(context, dev);
    // TODO
}


int main(int argc, char **argv)
{
    using clock = std::chrono::high_resolution_clock;

    size_t n_darts = 100000;

    switch (argc)
    {
    default:
        print_usage();
        return 1;

    case 3:
        n_darts = std::atoi(argv[2]);

    case 2:
        double pi;
        auto mode = std::string(argv[1]);
        auto darts = throw_darts(n_darts);

        if (mode == "cpu") {
            auto start = clock::now();
            pi = calculate_pi_cpu(darts);
            auto end = clock::now();
            std::chrono::duration<double> diff = end - start;
            std::cout << "pi: " << pi << std::endl
                      << "(" << diff.count() << " s)" << std::endl;
        }

        else if (mode == "boost") {
            pi = calculate_pi_boost_compute(darts);
        }

        else {
            print_usage();
            return 1;
        }

        return 0;
    }
}
